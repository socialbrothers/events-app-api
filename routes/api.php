<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['middleware' => 'cors'], function ($api) {

    $api->group(['prefix' => 'authentication'], function ($api) {

        $api->post('login', 'App\Http\Controllers\AuthenticationController@login');

        $api->post('refresh', 'App\Http\Controllers\AuthenticationController@refresh');

        $api->post('logout', 'App\Http\Controllers\AuthenticationController@logout');

        $api->get('me', 'App\Http\Controllers\AuthenticationController@me');

    });

    $api->get('qrcode/{code}', 'App\Http\Controllers\ActivationController@generateQrCode');

    $api->post('activations/validate', 'App\Http\Controllers\ActivationController@validateActivation');

    $api->post('users', 'App\Http\Controllers\UserController@store');

    /*
     * Routes with authentication required...
     */
    $api->group(['middleware' => ['api.auth']], function ($api) {

        $api->get('me', 'App\Http\Controllers\AuthenticationController@me');

        $api->resource('users', 'App\Http\Controllers\UserController', ['except' => 'store']);

        $api->resource('events', 'App\Http\Controllers\EventController');

        $api->group(['prefix' => 'events/{event}'], function ($api) {

            $api->resource('metas', 'App\Http\Controllers\Events\MetaController');

            $api->resource('notifications', 'App\Http\Controllers\Events\NotificationController');

            $api->resource('activities', 'App\Http\Controllers\Events\ActivityController');

            $api->resource('medias', 'App\Http\Controllers\Events\MediaController');

        });

        $api->resource('addresses', 'App\Http\Controllers\AddressController');

        $api->resource('activations', 'App\Http\Controllers\ActivationController');

    });
});
