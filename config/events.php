<?php

return [

    /**
     * All properties regarding events will go here
     */
    'events' => [

        /**
         * These event meta's will generated for each event on creation
         */
        'default_metas' => [
            [
                'key'           => 'feature_image_media_id',
                'type'          => 'integer',
                'default_value' => '',
            ],
            [
                'key'           => 'entrance_fee',
                'type'          => 'decimal',
                'default_value' => '',
            ],
            [
                'key'           => 'description',
                'type'          => 'string',
                'default_value' => '',
            ],
            [
                'key'           => 'contact_phone',
                'type'          => 'string',
                'default_value' => '',
            ],
            [
                'key'           => 'twitter_search_query',
                'type'          => 'string',
                'default_value' => '',
            ]
        ],

    ],


];