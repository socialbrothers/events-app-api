@component('mail::message')
# Invitation Events App

You have been invited to the Events App!

<img src="{{ env('APP_URL') }}/qrcode/{{ $code }}">

@component('mail::button', ['url' => ''])
    Direct Registreren
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
