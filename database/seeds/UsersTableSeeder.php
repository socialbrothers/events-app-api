<?php

use App\Address;
use App\Event;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'     => 'Rens Philipsen',
            'phone'    => '0636501191',
            'birthday' => '1994-08-09',
            'email'    => 'rens@socialbrothers.nl',
            'password' => Hash::make('12345')
        ]);

        $address = new Address([
            'street'       => 'Ravenoord',
            'house_number' => '109',
            'zip_code'     => '3523 DB',
            'city'         => 'Utrecht',
            'country'      => 'Nederland'
        ]);

        $address->addressable()->associate($user);
        $address->save();

        $event = Event::where('type', 'community')->first();

        $user->events()->attach($event);
    }
}
