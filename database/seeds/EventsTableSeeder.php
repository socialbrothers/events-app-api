<?php

use App\Event;
use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $community = Event::create([
            'type'  => 'community',
            'title' => 'Test Community'
        ]);

        Event::create([
            'community_id' => $community->id,
            'type'         => 'event',
            'title'        => 'Test Event',
            'start_time'   => '2018-12-12 20:00:00',
            'end_time'     => '2018-12-13 03:00:00',
        ]);
    }
}
