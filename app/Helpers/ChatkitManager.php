<?php

namespace App\Helpers;

use App\Event;
use Chatkit\Chatkit;
use Chatkit\Exceptions\MissingArgumentException;

class ChatkitManager
{
    private $chatkit;

    /**
     * ChatkitManager constructor.
     * @throws MissingArgumentException
     */
    public function __construct()
    {
        try {
            $this->chatkit = new Chatkit([
                'instance_locator' => env('CHATKIT_INSTANCE_LOCATOR'),
                'key'              => env('CHATKIT_KEY')
            ]);
        } catch (MissingArgumentException $e) {
            throw $e;
        }
    }

    /**
     * Create room for Chatkit
     *
     * @param $name
     * @param bool $isPrivate
     * @return array
     * @throws MissingArgumentException
     */
    public function createRoom($name, $isPrivate = true)
    {
        $response = $this->chatkit->createRoom([
            'creator_id' => 'admin',
            'name'       => $name,
            'private'    => $isPrivate,
        ]);

        $room = $response['body'];

        return $room;
    }

    /**
     * Delete room for Chatkit
     *
     * @param $id
     * @return array
     * @throws MissingArgumentException
     */
    public function deleteRoom($id)
    {
        $response = $this->chatkit->deleteRoom(['id' => $id]);

        return $response['body'];
    }

    /**
     * Create user for Chatkit
     *
     * @param $email
     * @param $name
     * @return mixed
     * @throws MissingArgumentException
     * @throws \Chatkit\Exceptions\TypeMismatchException
     */
    public function createUser($email, $name)
    {
        $response = $this->chatkit->createUser([
            'id'   => $email,
            'name' => $name
        ]);

        return $response['body'];
    }

    /**
     * Assign users (emails) to room
     *
     * @param array $emails
     * @param $roomId
     * @return mixed
     * @throws MissingArgumentException
     */
    public function assignUsersToRoom(array $emails, $roomId)
    {
        $response = $this->chatkit->addUsersToRoom([
            'user_ids' => $emails,
            'room_id'  => $roomId
        ]);

        return $response['body'];
    }

    /**
     * Remove users (emails) to room
     *
     * @param array $emails
     * @param $roomId
     * @return mixed
     * @throws MissingArgumentException
     */
    public function removeUsersToRoom(array $emails, $roomId)
    {
        $response = $this->chatkit->removeUsersFromRoom([
            'user_ids' => $emails,
            'room_id'  => $roomId
        ]);

        return $response['body'];
    }

    /**
     * Delete user for Chatkit
     *
     * @param $email
     * @return mixed
     * @throws MissingArgumentException
     */
    public function deleteUser($email)
    {
        $response = $this->chatkit->deleteUser(['id' => $email]);

        return $response['body'];
    }
}
