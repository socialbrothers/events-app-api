<?php

namespace App\Transformers;

use App\Address;
use League\Fractal\TransformerAbstract;

class AddressTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * List of resources default to include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    /**
     * A Fractal transformer.
     *
     * @param Address $address
     * @return array
     */
    public function transform(Address $address)
    {
        return [
            'id'           => $address->id,
            'street'       => $address->street,
            'house_number' => $address->house_number,
            'zip_code'     => $address->zip_code,
            'city'         => $address->city,
            'country'      => $address->country
        ];
    }
}
