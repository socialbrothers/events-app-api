<?php

namespace App\Transformers;

use App\Activation;
use League\Fractal\TransformerAbstract;

class ActivationTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * List of resources default to include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'event'
    ];

    /**
     * A Fractal transformer.
     *
     * @param Activation $activation
     * @return array
     */
    public function transform(Activation $activation)
    {
        return [
            'id'    => $activation->id,
            'email' => $activation->email,
            'code'  => $activation->code,
        ];
    }

    /**
     *
     * @param Activation $activation
     * @return \League\Fractal\Resource\Item
     */
    public function includeEvent(Activation $activation)
    {
        return $this->item($activation->event, new EventTransformer());
    }
}
