<?php

namespace App\Transformers;

use App\Media;
use League\Fractal\TransformerAbstract;

class MediaTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'user',
        'event',
    ];

    /**
     * List of resources default to include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    /**
     * A Fractal transformer.
     *
     * @param Media $media
     * @return array
     */
    public function transform(Media $media)
    {
        return [
            'id'         => $media->id,
            'path'       => $media->path,
            'thumb_path' => $media->thumb_path,
            'type'       => $media->type,
            'duration'   => $media->duration
        ];
    }

    public function includeUser(Media $media)
    {
        return $this->item($media->user, new UserTransformer());
    }

    public function includeEvent(Media $media)
    {
        return $this->item($media->event, new EventTransformer());
    }
}
