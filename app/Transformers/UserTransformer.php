<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'events',
        'activities',
        'address'
    ];

    /**
     * List of resources default to include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    /**
     * A Fractal transformer.
     *
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id'              => $user->id,
            'name'            => $user->name,
            'email'           => $user->email,
            'phone'           => $user->phone,
            'birthday'        => $user->birthday,
            'profile_picture' => $user->profile_picture,
        ];
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Item
     */
    public function includeAddress(User $user)
    {
        return $user->address ? $this->item($user->address, new AddressTransformer()) : null;
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Collection
     */
    public function includeEvents(User $user)
    {
        return $this->collection($user->events, new EventTransformer());
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Collection
     */
    public function includeActivities(User $user)
    {
        return $this->collection($user->activities, new ActivityTransformer());
    }
}
