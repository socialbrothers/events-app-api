<?php

namespace App\Transformers;

use App\Meta;
use League\Fractal\TransformerAbstract;

class MetaTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Meta $meta
     * @return array
     */
    public function transform(Meta $meta)
    {
        return [
            'id'    => $meta->id,
            'key'   => $meta->key,
            'value' => $meta->value,
        ];
    }
}
