<?php

namespace App\Transformers;

use App\ChatMessage;
use League\Fractal\TransformerAbstract;

class ChatMessageTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'event'
    ];

    /**
     * List of resources default to include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'user'
    ];

    /**
     * A Fractal transformer.
     *
     * @param ChatMessage $chatMessage
     * @return array
     */
    public function transform(ChatMessage $chatMessage)
    {
        return [
            'id'   => $chatMessage->id,
            'text' => $chatMessage->text,
        ];
    }

    /**
     * @param ChatMessage $chatMessage
     * @return \League\Fractal\Resource\Item
     */
    public function includeEvent(ChatMessage $chatMessage)
    {
        return $this->item($chatMessage->event, new EventTransformer());
    }

    /**
     * @param ChatMessage $chatMessage
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(ChatMessage $chatMessage)
    {
        return $this->item($chatMessage->user, new UserTransformer());
    }
}
