<?php

namespace App\Transformers;

use App\Event;
use League\Fractal\TransformerAbstract;

class EventTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'address',
        'activities'
    ];

    /**
     * List of resources default to include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'metas',
        'medias',
        'events',
        'users',
    ];

    /**
     * A Fractal transformer.
     *
     * @param Event $event
     * @return array
     */
    public function transform(Event $event)
    {
        return [
            'id'          => $event->id,
            'type'        => $event->type,
            'title'       => $event->title,
            'description' => $event->description,
            'start_time'  => $event->start_time,
            'end_time'    => $event->end_time,
            'code'        => $event->pivot ? $event->pivot->code : null
        ];
    }

    /**
     * This is the recursive relation, which is only available for communities as they contain events
     *
     * @param Event $event
     * @return \League\Fractal\Resource\Collection
     */
    public function includeEvents(Event $event)
    {
        return $this->collection($event->events, new EventTransformer());
    }

    /**
     * @param Event $event
     * @return \League\Fractal\Resource\Collection
     */
    public function includeUsers(Event $event)
    {
        return $this->collection($event->users, new UserTransformer());
    }

    /**
     * @param Event $event
     * @return \League\Fractal\Resource\Item
     */
    public function includeAddress(Event $event)
    {
        return $this->item($event->address, new AddressTransformer());
    }

    /**
     * @param Event $event
     * @return \League\Fractal\Resource\Collection
     */
    public function includeMetas(Event $event)
    {
        return $this->collection($event->metas, new MetaTransformer());
    }

    /**
     * @param Event $event
     * @return \League\Fractal\Resource\Collection
     */
    public function includeMedias(Event $event)
    {
        return $this->collection($event->medias, new MediaTransformer());
    }

    /**
     * @param Event $event
     * @return \League\Fractal\Resource\Collection
     */
    public function includeActivities(Event $event)
    {
        return $this->collection($event->activities, new ActivityTransformer());
    }
}
