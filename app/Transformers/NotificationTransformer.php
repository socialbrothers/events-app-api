<?php

namespace App\Transformers;

use App\Notification;
use League\Fractal\TransformerAbstract;

class NotificationTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * List of resources default to include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    /**
     * A Fractal transformer.
     *
     * @param Notification $notification
     * @return array
     */
    public function transform(Notification $notification)
    {
        return [
            'id'   => $notification->id,
            'text' => $notification->text,
        ];
    }
}
