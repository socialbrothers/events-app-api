<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Media extends Model
{

    /**
     * Handle events on model
     */
    public static function boot()
    {
        parent::boot();

        self::deleted(function ($model) {
            Media::deleteFiles($model);
        });
    }

    /**
     * The rules that are applied for validation
     *
     * @var array
     */
    public static $rules = [
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mediaable_type', 'mediaable_id', 'name', 'path', 'thumb_path', 'type', 'duration',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function mediaable()
    {
        return $this->morphTo();
    }

    private static function deleteFiles($model)
    {
        Storage::delete($model->path);
        Storage::delete($model->thumb_path);
    }
}
