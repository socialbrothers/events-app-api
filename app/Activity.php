<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    /**
     * The rules that are applied for validation
     *
     * @var array
     */
    public static $rules = [
        'text' => 'required',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'activitable_id', 'activitable_type', 'text',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function activitable()
    {
        return $this->morphTo();
    }
}
