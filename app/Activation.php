<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activation extends Model
{
    /**
     * The rules that are applied for validation
     *
     * @var array
     */
    public static $rules = [
        'event_id' => 'required|exists:events,id',
        'email'    => 'required|email|unique:activations',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id', 'email', 'code',
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

}
