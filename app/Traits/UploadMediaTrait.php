<?php

namespace App\Traits;

use App\Media;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;


trait UploadMediaTrait
{
    public function uploadMedias(Request $request)
    {
        if (!is_array($request->media)) {
            if (!$request->media) return false;
            return [$this->uploadMedia($request->media)];
        }
        $result = [];
        foreach ($request->media as $media) {
            $result[] = $this->uploadMedia($media);
        }
        return $result;
    }

    private function uploadMedia($media)
    {
        $storeIn = 'medias';

        $fileExtension = $media->getClientOriginalExtension();
        $path = $media->store($storeIn);
        $thumbPath = $this->uploadMediaThumb($path, $storeIn);
        $media = new Media();
        $media->user_id = auth()->user()->id;
        $media->type = $fileExtension;
        $media->path = $path;
        $media->thumb_path = $thumbPath;
        $media->save();
        return $media;
    }

    private function uploadMediaThumb($path, $storeIn)
    {
        $baseName = basename($path);
        $thumbPath = $storeIn . '/thumb/' . $baseName;
        Image::make($path)->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($thumbPath);
        return $thumbPath;
    }
}