<?php

namespace App\Mail;

use App\Activation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class ActivationCreated extends Mailable
{
    use Queueable, SerializesModels, SendGrid;

    /**
     * The activation instance.
     *
     * @var Activation
     */
    public $activation;

    /**
     * Create a new message instance.
     *
     * @param Activation $activation
     */
    public function __construct(Activation $activation)
    {
        $this->activation = $activation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Activation Created';

        return $this
            ->subject($subject)
            ->with(['code' => $this->activation->code])
            ->markdown('emails.activations.created')
            ->from(env('SEND_MAIL_FROM'))
            ->to($this->activation->email);
    }
}
