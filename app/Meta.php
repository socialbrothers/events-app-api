<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    /**
     * The rules that are applied for validation
     *
     * @var array
     */
    public static $rules = [
        'key'   => 'required|unique_with:metas,metaable_id,metaable_type',
        'type'  => 'required',
        'value' => 'required'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'metaable_type', 'metaable_id', 'key', 'type', 'value',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function metaable()
    {
        return $this->morphTo();
    }
}
