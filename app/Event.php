<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    /**
     * Handle events on model
     */
    public static function boot()
    {
        parent::boot();

        // On creation of model
        self::created(function ($model) {
            Event::generateMetas($model);
        });
    }

    /**
     * The rules that are applied for validation
     *
     * @var array
     */
    public static $rules = [
        'type'         => 'required',
        'community_id' => 'integer|nullable',
        'title'        => 'required',
        'start_time'   => 'required_if:type,event|date',
        'end_time'     => 'required_if:type,event|date|after:start_time'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'community_id', 'title', 'start_time', 'end_time',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function metas()
    {
        return $this->morphMany(Meta::class, 'metaable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('code');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany(Event::class, 'community_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function address()
    {
        return $this->morphOne(Address::class, 'addressable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function medias()
    {
        return $this->morphMany(Media::class, 'mediaable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function activities()
    {
        return $this->morphMany(Activity::class, 'activitable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activations()
    {
        return $this->hasMany(Activation::class);
    }

    /**
     * Set meta by key
     *
     * @param $key
     * @param $value
     * @return Model|\Illuminate\Database\Eloquent\Relations\MorphMany|null|object
     */
    public function setMeta($key, $value)
    {
        $meta = $this->getMeta($key);
        $meta->value = $value;
        $meta->save();
        return $meta;
    }

    /**
     * Get meta by key
     *
     * @param $key
     * @return Model|\Illuminate\Database\Eloquent\Relations\MorphMany|null|object
     */
    public function getMeta($key)
    {
        return $this->metas()->where('key', $key)->first();
    }

    /**
     * Check if this event is a community
     *
     * @return bool
     */
    public function isCommunity()
    {
        return $this->type === 'community';
    }

    /**
     * Generate event metas
     *
     * @param $model
     */
    public static function generateMetas($model)
    {
        $defaultMetas = config('events.events.default_metas');

        foreach ($defaultMetas as $meta) {
            // Check if exists. If not, then create
            if (!$model->getMeta($meta['key']))
                $model->metas()->create([
                    'key'   => $meta['key'],
                    'type'  => $meta['type'],
                    'value' => $meta['default_value']
                ]);
        }
    }
}
