<?php

namespace App\Http\Controllers;

use App\Activation;
use App\Traits\UploadMediaTrait;
use App\User;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    use UploadMediaTrait;

    protected $model;

    /**
     * LocationController constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return fractal($this->model->all(), new UserTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $activation = Activation::where('email', $request->email)->first();

        if (!$activation)
            throw ValidationException::withMessages(['email' => 'There was no activation found with this email.']);

        $requestArray = $request->toArray();

        if ($requestArray['password'])
            $requestArray['password'] = Hash::make($requestArray['password']);

        $user = $this->model->create($requestArray);

        if ($request->has('media')) {
            $medias = $this->uploadMedias($request);
            if (is_array($medias)) {
                foreach ($medias as $media) {
                    $media->associate($this->model);
                }
            }
            $this->model->medias = $medias;
        }

        $user->events()->attach($activation->event_id);

        if ($user)
            $activation->delete();

        return fractal($user, new UserTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        return fractal($this->model->findOrFail($user), new UserTransformer());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $user)
    {
        $user = $this->model->findOrFail($user);

        if ($request->has('password'))
            $request->password = Hash::make($request->password);

        $user->update($request->toArray());

        return fractal($user, new UserTransformer());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($user)
    {
        if ($this->model->findOrFail($user)->delete()) {
            return response()->json(['message' => 'User successfully deleted!'], 200);
        }
        return response()->json(['message' => 'User failed to delete!'], 404);
    }
}
