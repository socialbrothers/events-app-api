<?php

namespace App\Http\Controllers;

use App\Activation;
use App\Http\Requests\StoreActivationRequest;
use App\Mail\ActivationCreated;
use App\Transformers\ActivationTransformer;
use App\User;
use Dingo\Api\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use LaravelQRCode\Facades\QRCode;

class ActivationController extends Controller
{
    protected $model;

    /**
     * LocationController constructor.
     * @param Activation $model
     */
    public function __construct(Activation $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Activation[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return fractal($this->model->all(), new ActivationTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreActivationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreActivationRequest $request)
    {
        if ($user = User::whereEmail($request->email)->first())
            if ($user->events->contains($request->event_id))
                throw ValidationException::withMessages(['email' => 'User already exists with this email and event.']);

        $request['code'] = str_random(24);

        $activation = $this->model->create($request->toArray());

        if ($activation)
            Mail::send(new ActivationCreated($activation));

        return fractal($activation, new ActivationTransformer());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Activation $activation
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($activation)
    {
        if ($this->model->findOrFail($activation)->delete()) {
            return response()->json(['message' => 'Activation successfully deleted!'], 200);
        }
        return response()->json(['message' => 'Activation failed to delete!'], 404);
    }

    public function validateActivation(Request $request)
    {
        return Activation::where('code', $request->code)->firstOrFail();
    }

    public function generateQrCode($code)
    {
        return QRCode::text($code)->setSize(6)->svg();
    }
}
