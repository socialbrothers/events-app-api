<?php

namespace App\Http\Controllers;

use App\Address;
use App\Http\Requests\StoreAddressRequest;
use App\Http\Requests\UpdateAddressRequest;
use App\Transformers\AddressTransformer;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    protected $model;

    /**
     * LocationController constructor.
     * @param Address $model
     */
    public function __construct(Address $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Address[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return fractal($this->model->all(), new AddressTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAddressRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAddressRequest $request)
    {
        $address = $this->model->create($request->toArray());

        return fractal($address, new AddressTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Address $address
     * @return \Illuminate\Http\Response
     */
    public function show($address)
    {
        return fractal($this->model->findOrFail($address), new AddressTransformer());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAddressRequest $request
     * @param  \App\Address $address
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAddressRequest $request, $address)
    {
        $address = $this->model->findOrFail($address);

        $address->update($request->toArray());

        return fractal($address, new AddressTransformer());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Address $address
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($address)
    {
        if ($this->model->findOrFail($address)->delete()) {
            return response()->json(['message' => 'Address successfully deleted!'], 200);
        }
        return response()->json(['message' => 'Address failed to delete!'], 404);
    }
}
