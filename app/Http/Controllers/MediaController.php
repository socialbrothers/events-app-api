<?php

namespace App\Http\Controllers;

use App\Event;
use App\Media;
use App\Traits\UploadMediaTrait;
use App\Transformers\EventTransformer;
use App\Transformers\MediaTransformer;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    use UploadMediaTrait;

    protected $model;

    /**
     * LocationController constructor.
     * @param Event $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $model
     * @return Media[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index($model)
    {
        $this->model = $this->model->findOrFail($model);

        return fractal($this->model->medias, new MediaTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $model
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($model, Request $request)
    {
        $this->model = $this->model->findOrFail($model);

        if ($request->has('media')) {
            $medias = $this->uploadMedias($request);
            if (is_array($medias)) {
                foreach ($medias as $media) {
                    $media->associate($this->model);
                }
            }
            $this->model->medias = $medias;
        }

        return fractal($this->model->findOrFail($model), new EventTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param $model
     * @param  \App\Media $media
     * @return \Illuminate\Http\Response
     */
    public function show($model, $media)
    {
        $this->model = $this->model->findOrFail($model);

        return fractal($this->model->medias()->findOrFail($media), new MediaTransformer());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $model
     * @param  \App\Media $media
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($model, $media)
    {
        $this->model = $this->model->findOrFail($model);
        $media = $this->model->medias()->findOrFail($media);

        if ($media->delete()) {
            return response()->json(['message' => 'Media successfully deleted!'], 200);
        }
        return response()->json(['message' => 'Media failed to delete!'], 404);
    }
}
