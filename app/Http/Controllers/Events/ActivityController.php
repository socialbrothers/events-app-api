<?php

namespace App\Http\Controllers\Events;

use App\Activity;
use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreActivityRequest;
use App\Http\Requests\UpdateActivityRequest;
use App\Transformers\ActivityTransformer;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    protected $model;

    /**
     * LocationController constructor.
     * @param Event $model
     */
    public function __construct(Event $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $event
     * @return Activity[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index($event)
    {
        $this->model = $this->model->findOrFail($event);

        return fractal($this->model->activities, new ActivityTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $event
     * @param StoreActivityRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store($event, StoreActivityRequest $request)
    {
        $this->model = $this->model->findOrFail($event);

        $activity = $this->model->activities()->create($request->toArray());

        return fractal($activity, new ActivityTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param $event
     * @param  \App\Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function show($event, $activity)
    {
        $this->model = $this->model->findOrFail($event);

        return fractal($this->model->activities()->findOrFail($activity), new ActivityTransformer());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $event
     * @param UpdateActivityRequest $request
     * @param  \App\Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function update($event, UpdateActivityRequest $request, $activity)
    {
        $this->model = $this->model->findOrFail($event);

        $activity = $this->model->activities()->findOrFail($activity);

        $activity->update($request->toArray());

        return fractal($activity, new ActivityTransformer());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $event
     * @param  \App\Activity $activity
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($event, $activity)
    {
        $this->model = $this->model->findOrFail($event);

        if ($this->model->activities()->findOrFail($activity)->delete()) {
            return response()->json(['message' => 'Activity successfully deleted!'], 200);
        }
        return response()->json(['message' => 'Activity failed to delete!'], 404);
    }
}
