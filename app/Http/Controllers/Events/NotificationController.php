<?php

namespace App\Http\Controllers\Events;

use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNotificationRequest;
use App\Http\Requests\UpdateNotificationRequest;
use App\Notification;
use App\Transformers\NotificationTransformer;

class NotificationController extends Controller
{
    protected $model;

    /**
     * LocationController constructor.
     * @param Notification $model
     */
    public function __construct(Event $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $event
     * @return Notification[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index($event)
    {
        $this->model = $this->model->findOrFail($event);

        return fractal($this->model->notifications, new NotificationTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $event
     * @param StoreNotificationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store($event, StoreNotificationRequest $request)
    {
        $this->model = $this->model->findOrFail($event);

        $notification = $this->model->notifications()->create($request->toArray());

        return fractal($notification, new NotificationTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param $event
     * @param  \App\Notification $notification
     * @return \Illuminate\Http\Response
     */
    public function show($event, $notification)
    {
        $this->model = $this->model->findOrFail($event);

        return fractal($this->model->notifications()->findOrFail($notification), new NotificationTransformer());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $event
     * @param UpdateNotificationRequest $request
     * @param  \App\Notification $notification
     * @return \Illuminate\Http\Response
     */
    public function update($event, UpdateNotificationRequest $request, $notification)
    {
        $this->model = $this->model->findOrFail($event);

        $notification = $this->model->notifications()->findOrFail($notification);

        $notification->update($request->toArray());

        return fractal($notification, new NotificationTransformer());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $event
     * @param  \App\Notification $notification
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($event, $notification)
    {
        $this->model = $this->model->findOrFail($event);

        if ($this->model->notifications()->findOrFail($notification)->delete()) {
            return response()->json(['message' => 'Notification successfully deleted!'], 200);
        }
        return response()->json(['message' => 'Notification failed to delete!'], 404);
    }
}
