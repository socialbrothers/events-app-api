<?php

namespace App\Http\Controllers\Events;

use App\Event;
use App\Http\Controllers\Controller;
use App\Meta;
use App\Http\Requests\StoreMetaRequest;
use App\Http\Requests\UpdateMetaRequest;
use App\Transformers\MetaTransformer;

class MetaController extends Controller
{
    protected $model;

    /**
     * LocationController constructor.
     * @param Event $model
     */
    public function __construct(Event $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $event
     * @return Meta[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index($event)
    {
        $this->model = $this->model->findOrFail($event);

        return fractal($this->model->metas, new MetaTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $event
     * @param StoreMetaRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store($event, StoreMetaRequest $request)
    {
        $this->model = $this->model->findOrFail($event);

        $meta = $this->model->metas()->create($request->toArray());

        return fractal($meta, new MetaTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param $event
     * @param  \App\Meta $meta
     * @return \Illuminate\Http\Response
     */
    public function show($event, $meta)
    {
        $this->model = $this->model->findOrFail($event);

        return fractal($this->model->metas()->findOrFail($meta), new MetaTransformer());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $event
     * @param UpdateMetaRequest $request
     * @param  \App\Meta $meta
     * @return \Illuminate\Http\Response
     */
    public function update($event, UpdateMetaRequest $request, $meta)
    {
        $this->model = $this->model->findOrFail($event);

        $meta = $this->model->metas()->findOrFail($meta);

        $meta->update($request->only('value'));

        return fractal($meta, new MetaTransformer());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $event
     * @param  \App\Meta $meta
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($event, $meta)
    {
        $this->model = $this->model->findOrFail($event);

        if ($this->model->metas()->findOrFail($meta)->delete()) {
            return response()->json(['message' => 'Meta successfully deleted!'], 200);
        }
        return response()->json(['message' => 'Meta failed to delete!'], 404);
    }
}
