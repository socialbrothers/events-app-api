<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests\StoreEventRequest;
use App\Http\Requests\UpdateEventRequest;
use App\Traits\UploadMediaTrait;
use App\Transformers\EventTransformer;

class EventController extends Controller
{
    use UploadMediaTrait;

    protected $model;

    /**
     * LocationController constructor.
     * @param Event $model
     */
       public function __construct(Event $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Event[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return fractal(auth()->user()->events, new EventTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEventRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEventRequest $request)
    {
        $event = $this->model->create($request->toArray());

        if ($request->has('media')) {
            $medias = $this->uploadMedias($request);
            if (is_array($medias)) {
                foreach ($medias as $media) {
                    $media->associate($event);
                }
            }
            $event->medias = $medias;
        }

        return fractal($event, new EventTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event $event
     * @return \Illuminate\Http\Response
     */
    public function show($event)
    {
        return fractal($this->model->findOrFail($event), new EventTransformer());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEventRequest $request
     * @param  \App\Event $event
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEventRequest $request, $event)
    {
        $event = $this->model->findOrFail($event);

        $event->update($request->toArray());

        return fractal($event, new EventTransformer());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event $event
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($event)
    {
        if ($this->model->findOrFail($event)->delete()) {
            return response()->json(['message' => 'Event successfully deleted!'], 200);
        }
        return response()->json(['message' => 'Event failed to delete!'], 404);
    }
}
