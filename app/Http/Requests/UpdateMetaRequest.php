<?php

namespace App\Http\Requests;

use App\Meta;
use Dingo\Api\Http\FormRequest;

class UpdateMetaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Meta::$rules;

        unset($rules['key']);

        return $rules;
    }
}
