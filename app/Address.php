<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The rules that are applied for validation
     *
     * @var array
     */
    public static $rules = [
        'street'       => 'required',
        'house_number' => 'required',
        'zip_code'     => 'required',
        'city'         => 'required',
        'country'      => 'required',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'addressable_type', 'addressable_id', 'label', 'street', 'house_number', 'zip_code', 'city', 'country'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function addressable()
    {
        return $this->morphTo();
    }
    
}
